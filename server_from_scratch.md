# SERVER IONOS SETUP FROM SCRATCH

## FIRST STEPS
```bash
ssh root@IP_ADRESS (or DNS)
```

### SOME USEFUL INSTALLATIONS
```bash
apt update && apt upgrade
apt install git neofetch tmux net-tools tree
apt install colordiff sshfs
```

### USER CREATION
```bash
useradd -s /bin/bash -m -G sudo <name> 
```
- -s: it specifies the login shell
- -m creates the home directory 
- -G sudo: specifies that 'sudo' operation is allowed


### SETTING UP SSH LOGIN WITH A SSH KEY
From the user you want to log in as:
- make sure you are in the home directory ("cd").
- cd .ssh
- nano authorized_keys
paste the own public key - it can be obtained by the home of the own PC with “cat .ssh/id_rsa.pub”

Now, connect with ssh <name>@IP_adress and you will find on the user.
Eventually, by specifying the key file ssh <name>@IP_adress -i ~/.ssh/id_rsa


### INSTALL VSCODE AND TUNNEL
[**Guide**](https://code.visualstudio.com/docs/setup/linux) TL;DR “sudo snap install --classic code”

- From VSCode open on Windows: ctrl-shift-p, ssh add host, and enter username and password
- Alternatively: ctrl-shift-p, open ssh configuration, and add something like:
> Host ionos

> HostName IP

> User <name>

> IdentityFile C:\\Users\\<name>\\.ssh\\id_rsa


- Regarding the previous key file, open a Windows PowerShell
    - Create a key (ssh-keygen, press Enter a few times)
    - or copy the existing key (cat .ssh/id_rsa.pub)


## INSTALLING APACHE
```bash
USEFUL LINKS:
- https://www.ionos.com/digitalguide/server/configuration/install-apache-on-ubuntu/
- https://forum.vestacp.com/viewtopic.php?t=4854
- https://ubuntu.com/tutorials/install-and-configure-apache?gad_source=1&gclid=CjwKCAjwoa2xBhACEiwA1sb1BN8gT3l87vmPTnaVIy9npY3wCm94Xk-DOMAR107sMbRC7vpwKAiMJBoCZiYQAvD_BwE#4-setting-up-the-virtualhost-configuration-file
```

TL;DR
```bash
- sudo apt install apache2
```
Default page is /var/www/html/index.html
On ionos, resulded to be necessary “a2enmod actions”. It is compulsory to open the HTTP/HTTPS ports to be able to see the website. See section below:

## Firewall UFW - Open ports
[**Guide**](https://www.digitalocean.com/community/tutorials/how-to-set-up-a-firewall-with-ufw-on-ubuntu)

It is crucial to allow ssh before activating the firewall, otherwise won’t be able to connect again. 

```bash
sudo ufw default deny incoming
sudo ufw default allow outgoing

sudo ufw allow OpenSSH
sudo ufw allow ssh
sudo ufw allow 22

sudo ufw enable

sudo ufw allow http
sudo ufw allow https
sudo ufw allow ‘Apache Full’
```



## INSTALLING ANACONDA
- Download the anaconda installer (a link provided via mail might be necessary)
```bash
wget https://repo.anaconda.com/archive/Anaconda3-2024.02-1-Linux-x86_64.sh
```

- **INSIDE A TMUX SESSION** (“tmux new-session -s ana”), run:
```bash
bash Anaconda3-2024.02-1-Linux-x86_64.sh
```
- accept the terms by typing “yes”, confirm the installation path
**very important**: at the end type **yes** for running the initialization. 

- If during the installation accidentally press enter, I don’t know how to re-init again.
- Restart the terminal


## PREPARE SERVER FOR DOCKING PIPELINE  

### MGLtools
Create Anaconda environment with Python 2.7
```bash
conda create -n "py27" python=2.7
conda activate py27
conda install -c bioconda mgltools
```

This allows to call and have "prepare_ligand4.py" and "prepare_receptor4.py" in the path
```bash
cd ~/anaconda3/envs/py27/bin
ln -s  ../MGLToolsPckgs/AutoDockTools/Utilities24/prepare_pdb_split_alt_confs.py prepare_pdb_split_alt_confs.py
```

## Environment MOLE for PyMOL, MGLtools etc...
Pymol resulted in severe problems. We decided to go for [**PyMOL OpenSource**](https://anaconda.org/conda-forge/pymol-open-source)

```bash
conda create -n "MOLE" python=3.8.16
conda activate MOLE
conda install conda-forge::pymol-open-source
conda install -c bioconda mgltools
conda install numpy scipy pandas
```

### INSTALLING AUTODOCK VINA
[**Source**](https://vina.scripps.edu/downloads/)
```bash
wget https://vina.scripps.edu/wp-content/uploads/sites/55/2020/12/autodock_vina_1_1_2_linux_x86.tgz
tar zxvf autodock_vina_1_1_2_linux_x86.tgz
```

Then, add in the .bashrc
```bash
export PATH="$PATH:/home/pitoneverde/autodock_vina_1_1_2_linux_x86/bin”
```


## SCRAPING ENVIRONMENT
```bash
conda create -n SCRAP python
conda activate SCRAP
conda install -c anaconda Beautifulsoup4
conda install -c conda-forge httplib2 
conda install requests numpy scipy pandas matplotlib tqdm
```


